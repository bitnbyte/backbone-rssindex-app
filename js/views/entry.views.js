// js/views/entry.views.js

var EntryListItemView = Backbone.View.extend({
	tagName: 'li',
	className: 'entry-list-item',
	template: _.template($('#entry-list-item-tmpl').html()),
	render: function() {
		var html = this.template(this.model.toJSON());
		this.$el.html(html);
		return this;
	}
});

var EntryListView = Backbone.View.extend({
	el: '#app',
	initialize: function() {
		this.listenTo(this.collection, 'sync', this.render);
	},
	render: function() {
		var $list = this.$('ul.entry-list').empty();
		this.collection.each(function(model) {
			var item = new EntryListItemView({model: model});
			$list.append(item.render().$el);
		}, this);
		return this;
	}
});
