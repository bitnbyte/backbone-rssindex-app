// js/views/blog.views.js

var BlogListItemView = Backbone.View.extend({
	tagName: 'li',
	className: 'blog-list-item',
	template: _.template($('#blog-list-item-tmpl').html()),
	render: function() {
		var html = this.template(this.model.toJSON());
		this.$el.html(html);
		return this;
	},
	events: {
		'click a': 'onChoose'
	},
	onChoose: function() {
		entryList.fetch({
			dataType: 'jsonp',
			url: 'https://ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=10&q=' + this.model.attributes.url
		});
	}
});

var BlogListView = Backbone.View.extend({
	el: '#app',
	initialize: function() {
		this.listenTo(this.collection, 'sync', this.render);
	},
	render: function() {
		var $list = this.$('ul.blog-list').empty();
		this.collection.each(function(model) {
			var item = new BlogListItemView({model: model});
			$list.append(item.render().$el);
		}, this);
		return this;
	}
});

