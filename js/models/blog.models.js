// js/models/blog.models.js

var BlogModel = Backbone.Model.extend({
	defaults: {
		contentSnippet: null,
		link: null,
		title: null,
		url: null
	}
});

var BlogCollection = Backbone.Collection.extend({
	url: 'https://ajax.googleapis.com/ajax/services/feed/find?v=1.0&num=7&q=Recipe%20Blogs',
	parse: function(data) {
		return data.responseData.entries;
	},
	model: BlogModel
});