// js/models/entry.models.js

var EntryModel = Backbone.Model.extend({
	defaults: {
		attributes: {
			title: null,
			link: null,
			contentSnippet: null
		}
	}
});

var EntryCollection = Backbone.Collection.extend({
	parse: function(data) {
		return data.responseData.feed.entries
	},
	model: EntryModel
});
