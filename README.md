# A Backbone.js RSS index experiment

A basic Backbone.js app for viewing latest posts of a few recipe blogs. Uses JSON from the Google Feeds API.

With Bower and Node installed run: `npm start`

## Online demo

[http://bitnbyte.bitbucket.org/backbone-rssindex-app](http://bitnbyte.bitbucket.org/backbone-rssindex-app)
